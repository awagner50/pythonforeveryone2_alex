# Python for Everyone 2: Alex's student repository

This repository holds code that is used in Python for Everyone 2 taught at Carlow University in Spring 2024

# Project 1: Python I Choose You

This project is a recap for last semester but heavily uses files, functions, conditionals, and more.

# Project 2: Pandas I Choose You

This project is a complete overhaul of the first project but using pandas and some additional steps.

# Week 3: Working With Pandas to Manage Pokemon Teams

This is when the class moved away from big projects and moved towards two weekly assignments
For this week I mostly was working with the pandas library.
The assignments had to deal with making a pokemon team from given data(csv files).

# Week 4: Working With Pandas to Manage Pokemon Teams

The assignments this week built upon last week assignments.
I'm still working with pandas and building a program which allows a user to manage a pokemon team.

# Week 5: Creating Graphs using Pandas, Seaborn, and Matplotlib.

This weeks assignment continues to work with pandas and the pokemon dataset. It also uses Seabron and matplotlib to graph.
The program draws multiple graphs together and is presented as a single set of data.

# Week 6: Using Jupyter Notebooks and more

These assignments will be about pulling everything I've been doing the past few weeks together.
These assignments are within Jupyter Notebooks since that is one of the main focuses of class this week.
I use pandas, seaborn, matplotlib, ipywidgets, requests, json, and more libraries.

# Week 7: Using Jupyter Notebooks and Regex

There was no week 7 assignments but I leanred regex and continued with Jupyter Notebooks.

# Week 8: Exception Handling and Logging

Week 8 assignments continue using pandas. However, the main focus is exception handling and logging.

# Week 9: Using flask

Week 9 assignment uses a flask server along with a python app. There will be two endpoint which accept POST, GET, and DELETE requests. 

# Week 10: Using flask and Using curl Requests

Week 10 assignments build upon the week 9 assignemnt. For week 10 assignments, I used windows cmd promt for curl requests along with adding more to the python app.

# Week 11: Using flask, curl Requests, and now SQLite

Week 11 assignments build upon the previous two weeks of assignemnts. Starting in week 11, I use SQLite to connect and store data in a database using an api.

# Week 12: Using flask, curl Requests, and now SQLite

Week 12 assignments build upon the previous three weeks of assignemnts. Week 12 is simliar to week 11, but this week goes a little bit more in depth into the database and sql side. 

# Final Project: Team Battle

The final project takes aspects from every assignemnt and thing I leanred from the class.
This project consists of me building an API that allows me to build out teams of
pokemon so that they can compete against each other. This will consist of a number
of endpoints that allow the managing of all actors involved and storing that information in a
database so that it can be recalled later.
